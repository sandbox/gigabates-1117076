
***********
* README: *
***********

DESCRIPTION:
------------
This module provide a facet for Faceted Search to browse Ubercart products by 
their available attribute options. The module does not require any configuration.

REQUIREMENTS:
-------------
UC Attribute Facet requires Faceted Search and Ubercart.

INSTALLATION:
-------------
1. Place the entire uc_attribute_facet directory into your Drupal sites/all/modules/
   directory.

2. Enable the uc_price_facet module


Author:
-------
Graham Bates
info@grahambates.com