<?php
// $Id$

/**
 * @file
 * Provides a facet for searching content by UC Attribute.
 */

require_once('./'. drupal_get_path('module', 'faceted_search') .'/faceted_search.inc');

/**
 * Implementation of hook_faceted_search_collect().
 */
function uc_attribute_facet_faceted_search_collect(&$facets, $domain, $env_id, $selection, $arg = NULL) {
  $result = db_query("SELECT aid, label FROM {uc_attributes}");

  switch ($domain) {
    case 'facets':
      $attributes = _uc_attribute_facet_get_attributes();

      foreach ($attributes as $aid => $attribute) {
        if (!isset($selection) || isset($selection['uc_attribute'][$aid])) {
          $facets[] = new uc_attribute_facet($attribute);
        }
      }
      break;
    case 'text':
      // Scan the given search text for a 'uc_attribute:{path,path,path,...}' token,
      // and create facets from it.
      if ($found_text = search_query_extract($arg, 'uc_attribute')) {
        $attributes = _uc_attribute_facet_get_attributes();
        // Extract separate facets
        $paths = explode(',', $found_text);
        foreach ($paths as $path_index => $oids) {
          $oids = explode('.', $oids); // Extract path of oids
          $path = array(); // Array to collect path of categories
          foreach ($oids as $oid) {
            if (!is_numeric($oid) || $oid <= 0) {
              break; // Invalid oid
            }
            $option = uc_attribute_option_load($oid);
            if (!$option) {
              break; // No option for oid
            }

            if (!isset($attributes[$option->aid]) || (isset($selection) && !isset($selection['uc_attribute'][$option->aid]))) {
              break; // Options's attribute not allowed.
            }

            $path[] = new uc_attribute_facet_category($option->oid, $option->name);
          }
          // If found some categories in the current path of aids, build a facet
          if (count($path) > 0) {
            $facets[] = new uc_attribute_facet($attributes[$option->aid], $path);
          }
        }

        // Remove the parsed token from the search text.
        $arg = search_query_insert($arg, 'uc_attribute');
      }
      return $arg;
  }
}

function _uc_attribute_facet_get_attributes() {
  $attributes = array();
  $result = db_query("SELECT aid, label FROM {uc_attributes}");
  while ($attribute = db_fetch_object($result)) {
    $attributes[$attribute->aid] = $attribute;
  }
  return $attributes;
}

class uc_attribute_facet extends faceted_search_facet {

  var $_attribute;

  /**
   * Constructor.
   */
  function uc_attribute_facet($uc_attribute = NULL, $active_path = array()) {
    $this->_attribute = $uc_attribute;
    parent::faceted_search_facet('uc_attribute', $active_path);
  }

  function get_id() {
    return $this->_attribute->aid;
  }

  function get_label() {
    return t($this->_attribute->label);
  }

  /**
   * Returns the available sort options for this facet.
   */
  function get_sort_options() {
    return array(
      'count' => t('Count'),
      'attribute' => t('Attribute')
    );
  }

  /**
   * Handler for the 'count' sort criteria.
   */
  function build_sort_query_count(&$query) {
    $query->add_orderby('count', 'DESC');
    $query->add_orderby('o.ordering', 'ASC');
  }

  /**
   * Handler for the 'attribute' sort criteria.
   */
  function build_sort_query_attribute(&$query) {
    $query->add_orderby('o.ordering', 'ASC');
  }

  /**
   * Returns the search text for this facet, taking into account this facet's
   * active path.
   */
  function get_text() {
    if ($category = $this->get_active_category()) {
      return $category->get_text();
    }
    return '';
  }

  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results.
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    $query->add_table('uc_product_options', 'nid', 'n', 'nid', 'po');
    $query->add_table('uc_attribute_options', 'oid', 'po', 'oid', 'o');
    $query->add_where('o.aid = %d', $this->_attribute->aid);
    $query->add_field('po', 'oid');
    $query->add_field('o', 'name', 'option_name');
    $query->add_groupby('po.oid'); // Needed for counting matching nodes.
    return TRUE;
  }

  /**
   * This factory method creates categories given query results that include the
   * fields selected in get_root_categories_query() or get_subcategories_query().
   *
   * @param $results
   *   $results A database query result resource.
   *
   * @return
   *   Array of categories.
   */
  function build_categories($results) {
    $categories = array();
    while ($result = db_fetch_object($results)) {
      $result->aid = $this->_attribute->aid;
      $categories[] = new uc_attribute_facet_category($result->po_oid, $result->option_name, $result->count);
    }
    return $categories;
  }
}

/**
 * An option for the attribute.
 */
class uc_attribute_facet_category extends faceted_search_category {
  var $_oid;
  var $_label;

  function uc_attribute_facet_category($oid, $label, $count = NULL) {
    $this->_oid = $oid;
    $this->_label = $label;
    parent::faceted_search_category($count);
  }

  /**
   * Return the label of this category.
   *
   * @param $html
   *   TRUE when HTML is allowed in the label, FALSE otherwise. Checking this
   *   flag allows implementors to provide a rich-text label if desired, and an
   *   alternate plain text version for cases where HTML cannot be used. The
   *   implementor is responsible to ensure adequate security filtering.
   */
  function get_label($html = FALSE) {
    return t($this->_label);
  }

  function get_text() {
    return $this->_oid;
  }

  /**
   * Updates a query for selecting nodes matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $query->add_table('uc_product_options', 'nid', 'n', 'nid', 'o' . $this->_oid);
    $query->add_where('o' . $this->_oid . '.oid = %d', $this->_oid);
  }
}
